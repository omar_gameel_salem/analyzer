###  ###

### Requirements to build and run the solution

* Java 8
* Maven

###Tests
```
cd analyzer
mvn clean surefire-report:report
```
report will be at /target/site/surefire-report.html

###Run
```
cd analyzer
mvn clean package
java -jar target/analyzer-1.0-0.jar
```