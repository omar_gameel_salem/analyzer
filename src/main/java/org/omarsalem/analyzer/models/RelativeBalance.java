package org.omarsalem.analyzer.models;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class RelativeBalance {
    private BigDecimal amount;
    private int transactionsCount;

    @Override
    public String toString() {
        BigDecimal amount = getAmount();
        String sign = "";
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            sign = "-";
            amount = amount.negate();
        }
        return String.format("Relative balance for the period is: %s$%s\n", sign, amount) +
                String.format("Number of transactions included is: %s", transactionsCount);
    }
}
