package org.omarsalem.analyzer.models;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class Transaction {
    private String transactionId;
    private String fromAccountId;
    private String toAccountId;
    private LocalDateTime createAt;
    private BigDecimal amount;
    private TransactionType transactionType;
    private String relatedTransaction;

    public Transaction() {
    }

    public Transaction(String transactionId, String fromAccountId, String toAccountId, LocalDateTime createAt, BigDecimal amount, TransactionType transactionType) {
        this.transactionId = transactionId;
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.createAt = createAt;
        this.amount = amount;
        this.transactionType = transactionType;
    }

    public Transaction(String transactionId, String fromAccountId, String toAccountId, LocalDateTime createAt, BigDecimal amount, TransactionType transactionType, String relatedTransaction) {
        this(transactionId, fromAccountId, toAccountId, createAt, amount, transactionType);
        this.relatedTransaction = relatedTransaction;
    }
}
