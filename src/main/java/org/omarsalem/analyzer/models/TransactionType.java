package org.omarsalem.analyzer.models;

public enum TransactionType {
    PAYMENT, REVERSAL
}
