package org.omarsalem.analyzer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Utils {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

    public static LocalDateTime parseDate(String date) {
        return LocalDateTime.parse(date, DATE_TIME_FORMATTER);
    }
}
