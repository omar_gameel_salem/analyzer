package org.omarsalem.analyzer;

import org.omarsalem.analyzer.dal.TransactionsRepo;
import org.omarsalem.analyzer.dal.TransactionsRepoCSV;
import org.omarsalem.analyzer.models.RelativeBalance;
import org.omarsalem.analyzer.services.TransactionsService;
import org.omarsalem.analyzer.services.TransactionsServiceImpl;

import java.time.LocalDateTime;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("csv file path:");
        String filePath = scanner.nextLine();

        System.out.println("account Id:");
        String accountId = scanner.nextLine();

        System.out.println("from:");
        LocalDateTime from = Utils.parseDate(scanner.nextLine());

        System.out.println("to:");
        LocalDateTime to = Utils.parseDate(scanner.nextLine());

        TransactionsRepo transactionsRepo = new TransactionsRepoCSV(filePath);
        TransactionsService transactionsService = new TransactionsServiceImpl(transactionsRepo);
        final RelativeBalance relativeBalance = transactionsService.getBalance(accountId, from, to);
        System.out.println(relativeBalance);
    }
}
