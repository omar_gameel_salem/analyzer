package org.omarsalem.analyzer.dal;

import org.omarsalem.analyzer.Utils;
import org.omarsalem.analyzer.models.Transaction;
import org.omarsalem.analyzer.models.TransactionType;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TransactionsRepoCSV implements TransactionsRepo {
    private static final int TRANSACTION_ID = 0,
            FROM_ACCOUNT_ID = 1,
            TO_ACCOUNT_ID = 2,
            CREATED_AT = 3,
            AMOUNT = 4,
            TRANSACTION_TYPE = 5,
            RELATED_TRANSACTION = 6;
    private final String fileName;

    public TransactionsRepoCSV(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<Transaction> getRecords() {
        List<Transaction> records = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(fileName))) {
            int i = 0;
            while (scanner.hasNextLine()) {
                final String line = scanner.nextLine();
                if (i != 0) {
                    final Transaction transaction = parseRecord(line);
                    records.add(transaction);
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return records;
    }

    private Transaction parseRecord(String s) {
        final String[] arr = s.split(",");
        Transaction transaction = new Transaction();
        final BigDecimal amount = new BigDecimal(arr[AMOUNT].trim());
        final String rawDate = arr[CREATED_AT].trim();
        final LocalDateTime createAt = Utils.parseDate(rawDate);
        final String fromAccountId = arr[FROM_ACCOUNT_ID].trim();
        final String toAccountId = arr[TO_ACCOUNT_ID].trim();
        final TransactionType transactionType = TransactionType.valueOf(arr[TRANSACTION_TYPE].trim());
        final String transactionId = arr[TRANSACTION_ID].trim();
        transaction.setTransactionId(transactionId);
        transaction.setAmount(amount);
        transaction.setCreateAt(createAt);
        transaction.setFromAccountId(fromAccountId);
        transaction.setToAccountId(toAccountId);
        transaction.setTransactionType(transactionType);
        if (arr.length == 7) {
            final String relatedTransaction = arr[RELATED_TRANSACTION].trim();
            transaction.setRelatedTransaction(relatedTransaction);
        }
        return transaction;
    }
}
