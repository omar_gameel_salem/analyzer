package org.omarsalem.analyzer.dal;

import org.omarsalem.analyzer.models.Transaction;

import java.util.List;

public interface TransactionsRepo {
    List<Transaction> getRecords();
}
