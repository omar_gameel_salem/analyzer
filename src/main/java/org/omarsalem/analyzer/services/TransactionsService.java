package org.omarsalem.analyzer.services;

import org.omarsalem.analyzer.models.RelativeBalance;

import java.time.LocalDateTime;

public interface TransactionsService {
    RelativeBalance getBalance(String accountId, LocalDateTime from, LocalDateTime to);
}
