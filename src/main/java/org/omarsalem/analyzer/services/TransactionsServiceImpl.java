package org.omarsalem.analyzer.services;

import org.omarsalem.analyzer.dal.TransactionsRepo;
import org.omarsalem.analyzer.models.RelativeBalance;
import org.omarsalem.analyzer.models.Transaction;
import org.omarsalem.analyzer.models.TransactionType;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TransactionsServiceImpl implements TransactionsService {
    private final TransactionsRepo transactionsRepo;

    public TransactionsServiceImpl(TransactionsRepo transactionsRepo) {
        this.transactionsRepo = transactionsRepo;
    }

    @Override
    public RelativeBalance getBalance(String accountId, LocalDateTime from, LocalDateTime to) {
        final List<Transaction> accountTransactions = transactionsRepo
                .getRecords()
                .stream()
                .filter(t -> t.getFromAccountId().equals(accountId) || t.getToAccountId().equals(accountId))
                .collect(Collectors.toList());

        final Set<String> reversalTransactionsIds = accountTransactions
                .stream()
                .filter(t -> t.getTransactionType().equals(TransactionType.REVERSAL))
                .map(t -> t.getRelatedTransaction())
                .collect(Collectors.toSet());
        accountTransactions.removeIf(t -> t.getTransactionType().equals(TransactionType.REVERSAL) || reversalTransactionsIds.contains(t.getTransactionId()));
        final List<Transaction> filteredTransaction = accountTransactions
                .stream()
                .filter(t -> t.getCreateAt().isAfter(from) && t.getCreateAt().isBefore(to))
                .collect(Collectors.toList());
        final BigDecimal sum = sum(filteredTransaction, accountId);
        return new RelativeBalance() {{
            setAmount(sum);
            setTransactionsCount(filteredTransaction.size());
        }};
    }

    private BigDecimal sum(List<Transaction> filteredTransaction, String accountId) {
        BigDecimal result = BigDecimal.ZERO;
        for (Transaction transaction : filteredTransaction) {
            BigDecimal amount = transaction.getAmount();
            if (transaction.getFromAccountId().equals(accountId)) {
                amount = amount.negate();
            }
            result = result.add(amount);
        }
        return result;
    }
}
