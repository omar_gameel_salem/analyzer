package org.omarsalem.analyzer.dal;

import org.junit.Assert;
import org.junit.Test;
import org.omarsalem.analyzer.models.Transaction;

import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;

import static org.omarsalem.analyzer.models.TransactionType.PAYMENT;
import static org.omarsalem.analyzer.models.TransactionType.REVERSAL;

public class TransactionsRepoCSVTest {

    @Test
    public void getRecords() {
        //Arrange
        URL url = this.getClass().getResource("/transactions.csv");
        final String fileName = url.getPath();
        TransactionsRepoCSV target = new TransactionsRepoCSV(fileName);

        //Act
        final List<Transaction> records = target.getRecords();

        //Assert
        Assert.assertEquals(2, records.size());
        final Transaction firstTransaction = records.get(0);
        Assert.assertEquals(0, BigDecimal.valueOf(25).compareTo(firstTransaction.getAmount()));
        Assert.assertEquals(LocalDateTime.of(2018, 10, 20, 12, 47, 55), firstTransaction.getCreateAt());
        Assert.assertEquals("TX10001", firstTransaction.getTransactionId());
        Assert.assertEquals("ACC334455", firstTransaction.getFromAccountId());
        Assert.assertEquals("ACC778899", firstTransaction.getToAccountId());
        Assert.assertEquals(PAYMENT, firstTransaction.getTransactionType());

        final Transaction secondTransaction = records.get(1);
        Assert.assertEquals(REVERSAL, secondTransaction.getTransactionType());
        Assert.assertEquals("TX10002", secondTransaction.getRelatedTransaction());
    }
}