package org.omarsalem.analyzer.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.omarsalem.analyzer.Utils;
import org.omarsalem.analyzer.dal.TransactionsRepo;
import org.omarsalem.analyzer.models.RelativeBalance;
import org.omarsalem.analyzer.models.Transaction;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.omarsalem.analyzer.models.TransactionType.PAYMENT;
import static org.omarsalem.analyzer.models.TransactionType.REVERSAL;

public class TransactionsServiceImplTest {

    private TransactionsRepo transactionsRepo = mock(TransactionsRepo.class);
    private TransactionsServiceImpl target;

    @Before
    public void init() {
        target = new TransactionsServiceImpl(transactionsRepo);
    }

    @Test
    public void getBalance_Reversal_ReversedTransactionRemoved() {
        //Arrange
        final String accountId = "ACC334455";
        final LocalDateTime from = Utils.parseDate("20/10/2018 12:00:00");
        final LocalDateTime to = Utils.parseDate("20/10/2018 19:00:00");
        final List<Transaction> transactions = Arrays.asList(
                new Transaction("TX10001", "ACC334455", "ACC778899", Utils.parseDate("20/10/2018 12:47:55"), BigDecimal.valueOf(25.00), PAYMENT),
                new Transaction("TX10002", "ACC334455", "ACC998877", Utils.parseDate("20/10/2018 17:33:43"), BigDecimal.valueOf(10.50), PAYMENT),
                new Transaction("TX10003", "ACC998877", "ACC778899", Utils.parseDate("20/10/2018 18:00:00"), BigDecimal.valueOf(5.00), PAYMENT),
                new Transaction("TX10004", "ACC334455", "ACC998877", Utils.parseDate("20/10/2018 19:45:00"), BigDecimal.valueOf(10.50), REVERSAL, "TX10002"),
                new Transaction("TX10005", "ACC334455", "ACC778899", Utils.parseDate("21/10/2018 09:30:00"), BigDecimal.valueOf(7.25), PAYMENT));
        when(transactionsRepo.getRecords()).thenReturn(transactions);

        //Act
        final RelativeBalance relativeBalance = target.getBalance(accountId, from, to);

        //Assert
        Assert.assertEquals(0, relativeBalance.getAmount().compareTo(BigDecimal.valueOf(-25)));
        Assert.assertEquals(1, relativeBalance.getTransactionsCount());
    }

    @Test
    public void getBalance_Reversal_incomingBalance_positive() {
        //Arrange
        final String accountId = "ACC334455";
        final LocalDateTime from = Utils.parseDate("20/10/2018 12:00:00");
        final LocalDateTime to = Utils.parseDate("20/10/2018 19:00:00");
        final List<Transaction> transactions = Arrays.asList(
                new Transaction("TX10001", "ACC334455", "ACC778899", Utils.parseDate("20/10/2018 12:47:55"), BigDecimal.valueOf(10), PAYMENT),
                new Transaction("TX10002", "ACC998877", "ACC334455", Utils.parseDate("20/10/2018 17:33:43"), BigDecimal.valueOf(25), PAYMENT));
        when(transactionsRepo.getRecords()).thenReturn(transactions);

        //Act
        final RelativeBalance relativeBalance = target.getBalance(accountId, from, to);

        //Assert
        Assert.assertEquals(0, relativeBalance.getAmount().compareTo(BigDecimal.valueOf(15)));
        Assert.assertEquals(2, relativeBalance.getTransactionsCount());
    }
}