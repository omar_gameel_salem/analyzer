package org.omarsalem.analyzer.models;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class RelativeBalanceTest {

    @Test
    public void stringformattingTest_negative() {
        //Arrange
        final RelativeBalance relativeBalance = new RelativeBalance() {{
            setAmount(BigDecimal.valueOf(-25));
        }};

        //Act
        final String output = relativeBalance.toString();

        //Assert
        Assert.assertEquals("Relative balance for the period is: -$25\n" +
                "Number of transactions included is: 0", output);
    }

    @Test
    public void stringformattingTest_positive() {
        //Arrange
        final RelativeBalance relativeBalance = new RelativeBalance() {{
            setAmount(BigDecimal.valueOf(25));
        }};

        //Act
        final String output = relativeBalance.toString();

        //Assert
        Assert.assertEquals("Relative balance for the period is: $25\n" +
                "Number of transactions included is: 0", output);
    }

    @Test
    public void stringformattingTest_zero() {
        //Arrange
        final RelativeBalance relativeBalance = new RelativeBalance() {{
            setAmount(BigDecimal.ZERO);
        }};

        //Act
        final String output = relativeBalance.toString();

        //Assert
        Assert.assertEquals("Relative balance for the period is: $0\n" +
                "Number of transactions included is: 0", output);
    }
}